#include <stdio.h>

int main(int argc, char *argv[]){
	int i;
	
	if(argc < 2){
		printf("command argument unavailable");
		return 0;
		
	}
	
	for(i=1;i<argc;i++){
		
		FILE *fp;
		fp = fopen(argv[i],"r");
	
		int line = 0;
	
		char input[512];

		if(fp==NULL){
		printf("cannot open file \n");
		return 1;
		}
	
		while(fgets(input,512,fp)){
			line++;
			printf("%d. %s",line,input);
			printf("\n");
		}
	
		
	
	
		fclose(fp);
	}
	

	return 0;	
}
